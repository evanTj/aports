# Contributor: Daniel Isaksen <d@duniel.no>
# Contributor: Victor Schroeder <me@vschroeder.net>
# Contributor: Marlus Saraiva <marlus.saraiva@gmail.com>
# Maintainer: Michal Jirků <box@wejn.org>
pkgname=elixir
pkgver=1.15.0
pkgrel=0
pkgdesc="Elixir is a dynamic, functional language designed for building scalable and maintainable applications"
url="https://elixir-lang.org/"
# arm: build fails hundreds of times in a row on builders
# x86: fails tests
arch="noarch !x86 !armhf !armv7"
license="Apache-2.0"
depends="erlang-dev>=23.0"
makedepends="erlang-dialyzer"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/elixir-lang/elixir/archive/v$pkgver.tar.gz
	$pkgname-upstream-issue-12704.patch::https://github.com/elixir-lang/elixir/commit/73b65eca5af294a8afbed5bc73c178da18f0055c.diff
	$pkgname-upstream-issue-12677.patch::https://github.com/elixir-lang/elixir/commit/9b254e6830d0f44e26179582a16fef6f642df754.diff
	"

build() {
	# orig file caused by line offset of the issue 12704 diff
	rm -f bin/elixir.orig
	LANG="en_US.UTF-8" make
}

check() {
	set +e
	make test
	ret=$?
	set -e

	# test starts epmd, which then hangs presubmit pipeline.
	# so we kill it here.
	killall -q epmd

	return $ret
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install
}

sha512sums="
89af6b66fb06d3e2b847ec1e2667b17a78e9501a53397b2edffa530ac6933fb55f7654526bd76abb360c72cd11b023458cd26f36849b43efc91bd737ece7ee31  elixir-1.15.0.tar.gz
1860cb7a430aa5150d21717d97f49e1c45fe6dbe56c3af16a279b4fb56a329ee94f4459ee358198576c7bda2ed394395f9be80789de812e4c62d7e8cb4c9d7b4  elixir-upstream-issue-12704.patch
72a6699323c1242c234bf76cc6ac9fa3683d9f8fca704b6a309d04fef4add9593b8744c05d7d452aa5557cba5ed2548f370ddb4bfea2755855ebaa3a9733cb27  elixir-upstream-issue-12677.patch
"
