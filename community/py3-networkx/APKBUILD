# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer:
pkgname=py3-networkx
pkgver=2.8.8
pkgrel=3
pkgdesc="Software for complex networks"
url="https://networkx.github.io/"
arch="noarch"
license="BSD-3-Clause"
depends="
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-doc $pkgname-pyc"
checkdepends="
	xdg-utils
	py3-lxml
	py3-matplotlib
	py3-numpy
	py3-pandas
	py3-pydot
	py3-pygraphviz
	py3-pytest
	py3-pytest-xdist
	py3-scipy
	py3-yaml
	graphviz
	"
source="https://pypi.python.org/packages/source/n/networkx/networkx-$pkgver.tar.gz"
builddir="$srcdir/networkx-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl

	# Broken with graphviz
	# travellingsalesman:
	# https://github.com/networkx/networkx/issues/5913
	.testenv/bin/python3 -m pytest -n auto \
		--ignore networkx/drawing/tests/test_agraph.py \
		--ignore networkx/drawing/tests/test_pydot.py \
		--ignore networkx/algorithms/approximation/tests/test_traveling_salesman.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
c698ed2dcc4221662af585940cf5d6ce1ad97b6b8f6d84348ff1d6b233eb69a36f5fba362bc4bb3a08ec8716434690b52b1d66dc4122edb0f420eaa10023d9d4  networkx-2.8.8.tar.gz
"
